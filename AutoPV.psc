Scriptname AutoPV hidden

; FUNCTIONS
Function RegisterForAnimEvent_base(String EventName, Int Mode) global native
Function RegisterForAnimEvent(String EventName) global
	RegisterForAnimEvent_base(EventName, 1)
endFunction
Function UnregisterForAnimEvent(String EventName) global
	RegisterForAnimEvent_base(EventName, 0)
endFunction
Function UnregisterForAllAnimEvent() global native


Function ForceFirstPersonSmooth() global native
Function ForceThirdPersonSmooth() global native
Function ForceThirdPersonEX() global native
float Function GetCastTime(Form akForm) global native
float Function GetDeliveryType(Form akForm) global native

; EVENTS
Event OnAnimationEventEX(ObjectReference akSource, string asEventName)
endEvent

Event OnPlayerBehaviorState(float fBehavior, bool bSneakStart)
endEvent
