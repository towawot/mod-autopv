Scriptname AutoPV_Alias extends ReferenceAlias  

Import AutoPV

Actor aPlayer

float fDrawn
float fBow
float fCrossbow
float fStaff
float fSpell
float fSheathe
float fSneak_start
float fSneak_stop
float fSwim_start
float fSwim_stop

int[] iDrawnStyle
int[] iSpellDelivery
int[] iSpellDeliveryOption

bool bFP
bool bFP_f
bool bAction
bool bVATSMode
bool bSneakMode
bool bSwimMode
bool IsBeast

bool bVariableUpdate

Event OnInit()
	aPlayer = Game.Getplayer()
	ChangeState()
EndEvent

Event OnPlayerLoadGame()
	aPlayer = Game.Getplayer()
	ChangeState()
endEvent

Function VariableUpdate(int[] actions, bool[] styles, bool[] Deliverys, bool[] DeliveryOPtions)
	bVariableUpdate = true

	fDrawn = actions[0] as float
	fSheathe = actions[1] as float
	fBow = actions[2] as float
	fCrossbow = actions[3] as float
	fSpell = actions[4] as float
	fSneak_start = actions[5] as float
	fSneak_stop = actions[6] as float
	fStaff = actions[7] as float
	fSwim_start = actions[8] as float
	fSwim_stop = actions[9] as float

	iDrawnStyle = new Int[14]
	iDrawnStyle[0] = styles[0] as int
	iDrawnStyle[1] = styles[1] as int
	iDrawnStyle[2] = styles[2] as int
	iDrawnStyle[3] = styles[3] as int
	iDrawnStyle[4] = styles[4] as int
	iDrawnStyle[5] = styles[5] as int
	iDrawnStyle[6] = styles[6] as int
	iDrawnStyle[7] = styles[7] as int
	iDrawnStyle[8] = styles[8] as int
	iDrawnStyle[9] = styles[9] as int
	iDrawnStyle[10] = styles[10] as int
	iDrawnStyle[11] = styles[11] as int
	iDrawnStyle[12] = styles[12] as int
	iDrawnStyle[13] = styles[13] as int

	iSpellDelivery = new Int[5]
	iSpellDelivery[0] = Deliverys[0] as int
	iSpellDelivery[1] = Deliverys[1] as int
	iSpellDelivery[2] = Deliverys[2] as int
	iSpellDelivery[3] = Deliverys[3] as int
	iSpellDelivery[4] = Deliverys[4] as int

	iSpellDeliveryOption = new Int[5]
	iSpellDeliveryOption[0] = 0
	iSpellDeliveryOption[1] = 0
	iSpellDeliveryOption[2] = DeliveryOPtions[2] as int
	iSpellDeliveryOption[3] = 0
	iSpellDeliveryOption[4] = 0

	ChangeState()
endFunction

Event OnObjectEquipped(Form akBaseObject, ObjectReference akReference)
	ChangeState()
endEvent

Event OnAnimationEvent(ObjectReference akSource, string asEventName)
EndEvent

Event OnAnimationEventEX(ObjectReference akSource, string asEventName)
EndEvent


;---------------------------------------------------------------------- [Normal]

auto state Normalstate

	Event OnAnimationEvent(ObjectReference akSource, string asEventName)
		OnAnimationEventEX(akSource, asEventName)
	EndEvent
	
	Event OnAnimationEventEX(ObjectReference akSource, string asEventName)
		if (bSwimMode || bSneakMode)
			return
		endif

		bFP = !Game.GetCameraState()
		if (asEventName == "BeginWeaponDraw" || asEventName == "WeapOutLeftReplaceForceEquip" || asEventName == "WeapOutRightReplaceForceEquip")
			if (fDrawn)
				if (IsSwitch(iDrawnStyle))
					ForceSwitch(fDrawn)
				else
					ForceSwitch(fSheathe)
				endif
			endif
		elseif (asEventName == "weaponDraw" || asEventName == "MagicForceEquipLeft" || asEventName == "MagicForceEquipRight")
			if (fDrawn)
				if (IsSwitch(iDrawnStyle))
					ForceSwitch(fDrawn)
				else
					ForceSwitch(fSheathe)
				endif
			endif
		elseif (asEventName == "BeginWeaponSheathe" || asEventName == "Magic_Unequip_Out")
			ForceSwitch(fSheathe)
		endif
	endEvent

endState

;---------------------------------------------------------------------- [Bow]


state Bowstate
	Event OnBeginState()
		bAction = false
		bFP = !Game.GetCameraState()
	endEvent
	
	Event OnAnimationEvent(ObjectReference akSource, string asEventName)
		OnAnimationEventEX(akSource, asEventName)
	endEvent

	Event OnAnimationEventEX(ObjectReference akSource, string asEventName)
		if (bSwimMode || bSneakMode)
			return
		endif

		if (asEventName == "BeginWeaponDraw")
			if (fDrawn)
				if (IsSwitch(iDrawnStyle))
					ForceSwitch(fDrawn)
				else
					ForceSwitch(fSheathe)
				endif
			endif
		elseif (asEventName == "soundPlay.WPNBowPullLPM")
			if (!bAction)
				bAction = true
				SwitchStart(fbow)
			endif
		elseif (asEventName == "attackStop")
			if (bAction)
				WaitKillCinema()
				SwitchBack(fbow)
				bAction = False
			endif
		elseif (asEventName == "staggerPlayer")
			if (bAction)
				SwitchBack(fbow)
				bAction = false
			endif
		elseif (asEventName == "BeginWeaponSheathe")
			if (fSheathe && aPlayer.GetEquippedItemType(0) == 7)
				ForceSwitch(fSheathe)
			endif
			ChangeState()
		endif
	endEvent

	Event OnObjectUnEquipped(Form akBaseObject, ObjectReference akReference)
		if (bSwimMode || bSneakMode)
			return
		endif


		if (aPlayer.GetEquippedItemType(0) != 7)
			if (bAction)
				SwitchBack(fbow)
			endif
			ChangeState()
		endif
	endEvent
endState

;---------------------------------------------------------------------- [Crossbow]

state Crossbowstate
	Event OnBeginState()
		bAction = false
		bFP = !Game.GetCameraState()
	endEvent
	
	Event OnAnimationEvent(ObjectReference akSource, string asEventName)
		OnAnimationEventEX(akSource, asEventName)
	EndEvent
	
	Event OnAnimationEventEX(ObjectReference akSource, string asEventName)
		if (bSwimMode || bSneakMode)
			return
		endif

		if (asEventName == "BeginWeaponDraw")
			if (fDrawn)
				if (IsSwitch(iDrawnStyle))
					ForceSwitch(fDrawn)
				else
					ForceSwitch(fSheathe)
				endif
			endif
		elseif (asEventName == "bowDraw")
			if (!bAction)
				bAction = true
				SwitchStart(fCrossbow)
			endif
		elseif (asEventName == "attackStop")
			if (bAction)
				WaitKillCinema()
				SwitchBack(fCrossbow)
				bAction = False
			endif
		elseif (asEventName == "staggerPlayer")
			if (bAction)
				SwitchBack(fCrossbow)
				bAction = false
			endif
		elseif (asEventName == "BeginWeaponSheathe")
			if (fSheathe && aPlayer.GetEquippedItemType(0) == 12)
				ForceSwitch(fSheathe)
			endif
			ChangeState()
		endif
	endEvent

	Event OnObjectUnEquipped(Form akBaseObject, ObjectReference akReference)
		if (bSwimMode || bSneakMode)
			return
		endif

		if (aPlayer.GetEquippedItemType(0) != 12)
			if (!fSheathe)
				SwitchBack(fCrossbow)
			endif
			ChangeState()
		endif
	endEvent
endState

;---------------------------------------------------------------------- [Spell]

state Spellstate
	Event OnBeginState()
		bFP_f = False
		bFP = !Game.GetCameraState()
	endEvent

	Event OnAnimationEvent(ObjectReference akSource, string asEventName)
		OnAnimationEventEX(akSource, asEventName)
	EndEvent
	
	Event OnAnimationEventEX(ObjectReference akSource, string asEventName)
		if (bSwimMode || bSneakMode)
			return
		endif

		form fmObj
		form fmEff
		float DeliveryType
		
		int _Left = 0
		int _Right = 1

		if (asEventName == "BeginWeaponDraw")
			if (fDrawn)
				if (IsSwitch(iDrawnStyle))
					ForceSwitch(fDrawn)
				else
					ForceSwitch(fSheathe)
				endif
			endif
		elseif (asEventName == "weaponDraw")	; && !aPlayer.IsWeaponDrawn()
			if (fDrawn)
				if (IsSwitch(iDrawnStyle))
					ForceSwitch(fDrawn)
				else
					ForceSwitch(fSheathe)
				endif
			endif
		elseif (asEventName == "BeginCastLeft")
			if (!bFP_f)
				bFP_f = true
				bFP = !Game.GetCameraState()
			endif
			if (!bAction)
				bAction = true
				fmObj = aPlayer.GetEquippedObject(_Left)
				if (fmObj)
					int Hand0 = aPlayer.GetEquippedItemType(_Left)
					if (fStaff && Hand0 == 8)
						fmEff = (fmObj as Weapon).GetEnchantment()
					elseif (Hand0 == 9)
						fmEff = fmObj as Spell
					endif
					if (fmEff)
						DeliveryType = GetDeliveryType(fmEff)
						if (DeliveryType > -1)
							if (DeliveryType == 2 && iSpellDelivery[(DeliveryType as int)] && GetCastTime(fmEff))
								SwitchStart(fSpell)
							elseif (DeliveryType == 2 && iSpellDeliveryOption[(DeliveryType as int)] && GetCastTime(fmEff) == 0.0)
								SwitchStart(fSpell)
							elseif (iSpellDelivery[DeliveryType as int])
								SwitchStart(fSpell)
							endif
						endif
					endif
				endif
			endif
		elseif (asEventName == "BeginCastRight")
			if (!bFP_f)
				bFP_f = true
				bFP = !Game.GetCameraState()
			endif

			if (!bAction)
				bAction = true
				fmObj = aPlayer.GetEquippedObject(_Right)
				if (fmObj)
					int Hand1 = aPlayer.GetEquippedItemType(_Right)
					if (fStaff && Hand1 == 8)
						fmEff = (fmObj as Weapon).GetEnchantment()
					elseif (Hand1 == 9)
						fmEff = fmObj as Spell
					endif
					if (fmEff)
						DeliveryType = GetDeliveryType(fmEff)
						if (DeliveryType > -1)
							if (DeliveryType == 2 && iSpellDelivery[(DeliveryType as int)] && GetCastTime(fmEff))
								SwitchStart(fSpell)
							elseif (DeliveryType == 2 && iSpellDeliveryOption[(DeliveryType as int)] && GetCastTime(fmEff) == 0.0)
								SwitchStart(fSpell)
							elseif (iSpellDelivery[DeliveryType as int])
								SwitchStart(fSpell)
							endif
						endif
					endif
				endif
			endif
		elseif (asEventName == "staggerPlayer")
			if (bAction)
				SwitchBack(fSpell)
				bAction = false
			endif
		elseif (asEventName == "InterruptCast" || asEventName == "CastStop"  || asEventName == "IdleStop")
; 			bFP_f = false
			if (bAction)
				WaitKillCinema()
				SwitchBack(fSpell)
				bAction = False
			endif
;			if aPlayer.GetEquippedItemType(0) != 9 && aPlayer.GetEquippedItemType(1) != 9
;				ChangeState()
;			endif
		elseif (asEventName == "BeginWeaponSheathe")
			if (fSheathe)
				if (aPlayer.GetEquippedItemType(0) == 9)
					ForceSwitch(fSheathe)
				endif
			endif
			ChangeState()
		elseif (asEventName == "weaponSheathe")
			if (fSheathe)
				if (aPlayer.GetEquippedItemType(1) == 9)
					ForceSwitch(fSheathe)
				endif
			endif
			ChangeState()
		endif
	endEvent

	Event OnObjectUnEquipped(Form akBaseObject, ObjectReference akReference)
		if (bSwimMode || bSneakMode)
			return
		endif

		if (aPlayer.GetEquippedItemType(0) != 9 && aPlayer.GetEquippedItemType(1) != 9)
			if (bAction)
				SwitchBack(fSpell)
			endif
			ChangeState()
		endif
	endEvent
endState


;---------------------------------------------------------------------- [BeastState]

state BeastState
	Event OnAnimationEvent(ObjectReference akSource, string asEventName)
	EndEvent
	
	Event OnAnimationEventEX(ObjectReference akSource, string asEventName)
	endEvent

	Event OnObjectUnEquipped(Form akBaseObject, ObjectReference akReference)
	endEvent
endState

Event OnPlayerCameraState(int oldState, int newState)
	int VATS = 2
	if (bVATSMode && oldState == VATS)
		bVATSMode = false
	elseif (newState == VATS)
		bVATSMode = true
	EndIf
endEvent


Event OnPlayerBehaviorState(float fBehavior, bool bBehaviorStart)
	if (fBehavior == 1 || fBehavior == 2)
		if (!bSwimMode)
			if (bBehaviorStart && !bSneakMode)
				ForceSwitch(fSneak_start)
			elseif (!bBehaviorStart && bSneakMode)
				ForceSwitch(fSneak_stop)
			endif
			bSneakMode = bBehaviorStart
		endif
	elseif(fBehavior == 3 || fBehavior == 4)
		if (!bSneakMode)
			if (bBehaviorStart && !bSwimMode)
				ForceSwitch(fSwim_start)
			elseif (!bBehaviorStart && bSwimMode)
				ForceSwitch(fSwim_stop)
			endif
		endif
		bSwimMode = bBehaviorStart
	endif
endEvent

;---------------------------------------------------------------------- [Functions]
Function ForceSwitch(float f)
; 1.0=directFP 2.0=directTP 3.0=SmoothFP 4.0=SmoothTP
	if (f == 1.0)
		if (Game.GetCameraState())
			game.forceFirstperson()
		endif
	elseif (f == 2.0)
		if (!Game.GetCameraState())
			ForceThirdPersonEX()	;game.forcethirdperson()
		endif
	elseif (f == 3.0)
		if (Game.GetCameraState())
			ForceFirstPersonSmooth()
		endif
	elseif (f == 4.0)
		if (!Game.GetCameraState())
			ForceThirdPersonSmooth()
		endif
	endif
endFunction

Function SwitchStart(float f)
; 1.0=directFP 2.0=directTP 3.0=SmoothFP 4.0=SmoothTP
	if (f == 1.0 && !bFP)
		if (Game.GetCameraState())
			game.forceFirstperson()
		endif
	elseif (f == 2.0 && bFP)
		if (!Game.GetCameraState())
			ForceThirdPersonEX()	;	game.forcethirdperson()
		endif
	elseif (f == 3.0 && !bFP)
		if (Game.GetCameraState())
			ForceFirstPersonSmooth()
		endif
	elseif (f == 4.0 && bFP)
		if (!Game.GetCameraState())
			ForceThirdPersonSmooth()
		endif
	endif
endFunction

Function SwitchBack(float f)
; 1.0=directFP 2.0=directTP 3.0=SmoothFP 4.0=SmoothTP
	if (f == 1.0 && !bFP)
		if (!Game.GetCameraState())
			ForceThirdPersonEX()
		endif
	elseif (f == 2.0 && bFP)
		if (Game.GetCameraState())
			game.forceFirstperson()
		endif
	elseif (f == 3.0 && !bFP)
		if (!Game.GetCameraState())
			ForceThirdPersonSmooth()
		endif
	elseif (f == 4.0 && bFP)
		if (Game.GetCameraState())
			ForceFirstPersonSmooth()
		endif
	endif
endFunction

Function WaitKillCinema()
	int loop = 60
	while (bVATSMode && loop)
		utility.wait(0.1)
		loop -= 1
	endWhile
	if (!loop)
		debug.Notification("$AutoPVTimeout")
	endif
endFunction

bool Function IsSwitch(Int[] iStyle)
	int hand0 = aPlayer.GetEquippedItemType(0)
	if (hand0 == 7 || hand0 == 12)
		if (iStyle[hand0])
			return true
		endif
		return false
	endif
	int hand1 = aPlayer.GetEquippedItemType(1)
	if (iStyle[hand1])
		return true
	endif
	return false
endFunction

Function UnRegisterForAllAnimationEvent(ObjectReference ref)
	UnRegisterForAnimationEvent(ref, "BeginWeaponDraw")
	UnRegisterForAnimationEvent(ref, "BeginWeaponSheathe")
	UnRegisterForAnimationEvent(ref, "weaponDraw")
	UnRegisterForAnimationEvent(ref, "weaponSheathe")
	UnRegisterForAnimationEvent(ref, "soundPlay.WPNBowPullLPM")
	UnRegisterForAnimationEvent(ref, "bowDraw")
	UnRegisterForAnimationEvent(ref, "attackStop")
	UnRegisterForAnimationEvent(ref, "BeginCastRight")
	UnRegisterForAnimationEvent(ref, "BeginCastLeft")
	UnRegisterForAnimationEvent(ref, "InterruptCast")
	UnRegisterForAnimationEvent(ref, "CastStop")
	UnRegisterForAnimationEvent(ref, "IdleStop")
	UnRegisterForAnimationEvent(ref, "attackStop")
	UnRegisterForAnimationEvent(ref, "Magic_Unequip_Out")
endFunction

function ChangeState()
	string StateName = "Normalstate"
	if (bVariableUpdate)
		UnRegisterForAllAnimationEvent(aPlayer)
		UnRegisterForAllAnimEvent()
		UnregisterForCameraState()
	
		if (IsBeast)
			StateName = "BeastState"
		else
			if (fDrawn)
				RegisterForAnimationEvent(aPlayer, "BeginWeaponDraw")
				RegisterForAnimationEvent(aPlayer, "weaponDraw") ; Righthand only
			endif
			if (fSheathe)
				RegisterForAnimationEvent(aPlayer, "BeginWeaponSheathe")
				RegisterForAnimationEvent(aPlayer, "weaponSheathe") ; Righthand only
			endif
			
			if (fSneak_start)
				RegisterForAnimEvent("sneakStart")
			endif
			if (fSneak_stop)
				RegisterForAnimEvent("sneakStop")
			endif

			if (fSwim_start)
				RegisterForAnimEvent("swimStart")
			endif
			if (fSwim_stop)
				RegisterForAnimEvent("swimStop")
			endif
			
			int Hand0 = aPlayer.GetEquippedItemType(0)
			int Hand1 = aPlayer.GetEquippedItemType(1)
			
			if (fBow && Hand0 == 7)	;bow
				RegisterForAnimationEvent(aPlayer, "soundPlay.WPNBowPullLPM")
				RegisterForAnimationEvent(aPlayer, "attackStop")
				RegisterForAnimEvent("staggerPlayer")
				RegisterForCameraState()
				StateName = "Bowstate"
			elseif (fCrossbow && Hand0 == 12)	;crossbow
				RegisterForAnimationEvent(aPlayer, "bowDraw")
				RegisterForAnimationEvent(aPlayer, "attackStop")
				RegisterForAnimEvent("staggerPlayer")
				RegisterForCameraState()
				StateName = "Crossbowstate"
			elseif (fStaff && fSpell && (Hand0 == 8 || Hand1 == 8))	;staff
				RegisterForAnimationEvent(aPlayer, "BeginCastRight")
				RegisterForAnimationEvent(aPlayer, "BeginCastLeft")
				RegisterForAnimationEvent(aPlayer, "InterruptCast")
				RegisterForAnimationEvent(aPlayer, "CastStop")
				RegisterForAnimationEvent(aPlayer, "IdleStop")
				RegisterForAnimEvent("staggerPlayer")
				RegisterForCameraState()
				StateName = "Spellstate"
			elseif (fSpell && (Hand0 == 9 || Hand1 == 9))	;spell
				RegisterForAnimationEvent(aPlayer, "BeginCastRight")
				RegisterForAnimationEvent(aPlayer, "BeginCastLeft")
				RegisterForAnimationEvent(aPlayer, "InterruptCast")
				RegisterForAnimationEvent(aPlayer, "CastStop")
				RegisterForAnimationEvent(aPlayer, "IdleStop")
				RegisterForAnimEvent("staggerPlayer")
				RegisterForCameraState()
				StateName = "Spellstate"
			else
				RegisterForAnimationEvent(aPlayer, "Magic_Unequip_Out") ; spell
				RegisterForAnimEvent("WeapOutLeftReplaceForceEquip")
				RegisterForAnimEvent("WeapOutRightReplaceForceEquip")
				RegisterForAnimEvent("MagicForceEquipLeft")
				RegisterForAnimEvent("MagicForceEquipRight")
				StateName = "Normalstate"
			endif
		endif
; 		debug.Notification(StateName)
		gotostate(StateName)
	endif
endFunction

Event OnRaceSwitchComplete()
	if (aPlayer.HasKeywordString("ImmuneParalysis"))
		IsBeast = true
		ChangeState()
		return
	endif
	IsBeast = false
	ChangeState()
EndEvent
