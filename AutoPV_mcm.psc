Scriptname AutoPV_MCM extends SKI_ConfigBase  

AutoPV_Alias Property APV_Alias Auto

string[] sPvSelect
string[] sStaffSelect

int[] iPvStateID
int[] iPvState

Int[] iDrawStyleID
Bool[] bDrawStyle

Int[] iSpellDeliveryID
Bool[] bSpellDelivery

Int[] iSpellDeliveryOptionID
Bool[] bSpellDeliveryOption
	
event OnConfigInit()
	Pages = New String[2]
	Pages[0] = "$General"
	Pages[1] = "$Behavior"
	
	iPvStateID = new int[10]
	iPvState   = new int[10]
	
	iDrawStyleID = new int[14]
	bDrawStyle   = new Bool[14]
	bDrawStyle[0] = true
	bDrawStyle[1] = true
	bDrawStyle[2] = true
	bDrawStyle[3] = true
	bDrawStyle[4] = true
	bDrawStyle[5] = true
	bDrawStyle[6] = true
	bDrawStyle[7] = false
	bDrawStyle[8] = false
	bDrawStyle[9] = false
	bDrawStyle[10] = false
	bDrawStyle[11] = false
	bDrawStyle[12] = false
	bDrawStyle[13] = false

	iSpellDeliveryID = new int[5]
	bSpellDelivery   = new Bool[5]
	bSpellDelivery[0] = true
	bSpellDelivery[1] = true
	bSpellDelivery[2] = true
	bSpellDelivery[3] = true
	bSpellDelivery[4] = true
	
	iSpellDeliveryOptionID = new int[5]
	bSpellDeliveryOption   = new Bool[5]
	bSpellDeliveryOption[0] = true
	bSpellDeliveryOption[1] = true
	bSpellDeliveryOption[2] = true
	bSpellDeliveryOption[3] = true
	bSpellDeliveryOption[4] = true

	sPvSelect = new string[5]
	sPvSelect[0] = "$Do Nothing"
	sPvSelect[1] = "$1st Direct"
	sPvSelect[2] = "$3rd Direct"
	sPvSelect[3] = "$1st Smooth"
	sPvSelect[4] = "$3rd Smooth"

	sStaffSelect = new string[2]
	sStaffSelect[0] = "$Do not Include."
	sStaffSelect[1] = "$Include."
	
endEvent

int function GetVersion()
	return 1
endFunction

Event OnVersionUpdate(int a_version)

endEvent

Event OnPageReset(String a_Page)
	If (a_Page == "")
		LoadCustomContent("towawot/AutoPV.dds")
		Return
	Else
		UnloadCustomContent()
	EndIf

	If (a_Page == Pages[0])
; 		======================== LEFT ========================
		SetCursorFillMode(TOP_TO_BOTTOM)

		AddHeaderOption("$When acted")
		iPvStateID[0] = AddMenuOption("$Weapon Draw", sPvSelect[iPvState[0]])
		iPvStateID[1] = AddMenuOption("$Weapon Sheathe", sPvSelect[iPvState[1]])
		iPvStateID[5] = AddMenuOption("$Sneak start", sPvSelect[iPvState[5]])
		iPvStateID[6] = AddMenuOption("$Sneak stop", sPvSelect[iPvState[6]])

		AddHeaderOption("$Weapon Draw Option - Right Weapon type")
		iDrawStyleID[0] = AddToggleOption("$Fist", bDrawStyle[0])
		iDrawStyleID[1] = AddToggleOption("$One-handed sword", bDrawStyle[1])
		iDrawStyleID[2] = AddToggleOption("$One-handed dagger", bDrawStyle[2])
		iDrawStyleID[3] = AddToggleOption("$One-handed axe", bDrawStyle[3])
		iDrawStyleID[4] = AddToggleOption("$One-handed mace", bDrawStyle[4])
		iDrawStyleID[5] = AddToggleOption("$Two-handed sword", bDrawStyle[5])
		iDrawStyleID[6] = AddToggleOption("$Two-handed axe/mace", bDrawStyle[6])
		iDrawStyleID[7] = AddToggleOption("$Bow", bDrawStyle[7])
		iDrawStyleID[8] = AddToggleOption("$Staff", bDrawStyle[8])
		iDrawStyleID[9] = AddToggleOption("$MagicSpell", bDrawStyle[9])
; 		iDrawStyleID[10] = AddToggleOption("$Shield", bDrawStyle[10])
; 		iDrawStyleID[11] = AddToggleOption("$Torch", bDrawStyle[11])
		iDrawStyleID[12] = AddToggleOption("$Crossbow", bDrawStyle[12])
; 		iDrawStyleID[13] = AddToggleOption("$Other", bDrawStyle[13])

; 		======================== RIGHT ========================
		SetCursorPosition(1)

		AddHeaderOption("$While action")
		iPvStateID[2] = AddMenuOption("$Pull Bowstring", sPvSelect[iPvState[2]])
		iPvStateID[3] = AddMenuOption("$Aim Crossbow", sPvSelect[iPvState[3]])
		iPvStateID[4] = AddMenuOption("$Cast Spell", sPvSelect[iPvState[4]])
		iPvStateID[7] = AddTextOption("$(Include the Staff)", sStaffSelect[iPvState[7]])

		AddHeaderOption("$Cast Spell Option - Delivery type")
		iSpellDeliveryID[0] = AddToggleOption("$Self", bSpellDelivery[0])
		iSpellDeliveryID[1] = AddToggleOption("$Contact", bSpellDelivery[1])
		iSpellDeliveryID[2] = AddToggleOption("$Aimed", bSpellDelivery[2])
		iSpellDeliveryOptionID[2] = AddToggleOption("$Aimed - No cast time", bSpellDeliveryOption[2])
		iSpellDeliveryID[3] = AddToggleOption("$TargetActor", bSpellDelivery[3])
		iSpellDeliveryID[4] = AddToggleOption("$TargetLocation", bSpellDelivery[4])
	elseif (a_Page == Pages[1])
; 		======================== LEFT ========================
		SetCursorFillMode(TOP_TO_BOTTOM)

		AddHeaderOption("$When acted")
		iPvStateID[8] = AddMenuOption("$Swim start", sPvSelect[iPvState[8]])
		iPvStateID[9] = AddMenuOption("$Swim stop", sPvSelect[iPvState[9]])

; 		======================== RIGHT ========================
		SetCursorPosition(1)

	endif
endEvent

event OnOptionSelect(int a_option)
	bool continue = true
	string msg
	int index = iDrawStyleID.find(a_option)
	if (index > -1)
		if (index == 7 || index == 8 || index == 9 || index == 12)
			if (!bDrawStyle[index])
				if (index == 7 && iPvState[2])
					msg = "$Conflict1"
				elseif (index == 8 && iPvState[4] && iPvState[7])
					msg = "$Conflict2"
				elseif (index == 9 && iPvState[4])
					msg = "$Conflict3"
				elseif (index == 12 && iPvState[3])
					msg = "$Conflict4"
				endif
			endif
		endif

		if (msg)
			continue = ShowMessage(msg, true, "$Yes", "$No")
		endif
		if (continue)
			bDrawStyle[index] = !bDrawStyle[index]
			SetToggleOptionValue(a_option, bDrawStyle[index])
		endif
		return
	endif

	index = iSpellDeliveryID.find(a_option)
	if (index > -1)
		bSpellDelivery[index] = !bSpellDelivery[index]
		SetToggleOptionValue(a_option, bSpellDelivery[index])
		return
	endif

	index = iSpellDeliveryOptionID.find(a_option)
	if (index > -1)
		bSpellDeliveryOption[index] = !bSpellDeliveryOption[index]
		SetToggleOptionValue(a_option, bSpellDeliveryOption[index])
		return
	endif

	index = iPvStateID.find(a_option)
	if (index > -1)
		if (index == 2 || index == 3 || index == 4 || index == 7)
			if (!iPvState[index])
				if (index == 2 && bDrawStyle[7])
					msg = "$Conflict5"
				elseif (index == 3 && bDrawStyle[12])
					msg = "$Conflict6"
				elseif (index == 4 && bDrawStyle[9])
					msg = "$Conflict7"
				elseif (index == 7 && bDrawStyle[8])
					msg = "$Conflict8"
				endif
			endif
		endif

		if (msg)
			continue = ShowMessage(msg, true, "$Yes", "$No")
		endif
		if (continue)
			int iTemp = CycleTEXT(sStaffSelect, iPvState[index])
			SetTextOptionValue(a_option, sStaffSelect[iTemp])
			iPvState[index] = iTemp
		endif
		return
	endif
endEvent

event OnOptionMenuOpen(int a_option)
	bool continue = true
	string msg
	int index = iPvStateID.find(a_option)
	if (index > -1)
		if (index == 2 || index == 3 || index == 4 || index == 7)
			if (!iPvState[index])
				if (index == 2 && bDrawStyle[7])
					msg = "$Conflict5"
				elseif (index == 3 && bDrawStyle[12])
					msg = "$Conflict6"
				elseif (index == 4 && bDrawStyle[9])
					msg = "$Conflict7"
				elseif (index == 7 && bDrawStyle[8])
					msg = "$Conflict8"
				endif
			endif
		endif

		if (msg)
			continue = ShowMessage(msg, false, "$Yes")
		endif
		SetMenuDialogOptions(sPvSelect)
		SetMenuDialogStartIndex(iPvState[index])
		SetMenuDialogDefaultIndex(0)
		return
	endif
endEvent


event OnOptionMenuAccept(int a_option, int Optionindex)
	int index = iPvStateID.find(a_option)
	if (index > -1)
		iPvState[index] = Optionindex
		SetMenuOptionValue(iPvStateID[index], sPvSelect[iPvState[index]])
		return
	endif
endEvent

Event OnConfigClose()
	APV_Alias.VariableUpdate(iPvState, bDrawStyle, bSpellDelivery, bSpellDeliveryOption)
EndEvent

int Function CycleTEXT(string[] strs, int iTEXT)
	iTEXT += 1
	if (iTEXT >= strs.length)
		iTEXT = 0
	endif
	return iTEXT
endFunction
